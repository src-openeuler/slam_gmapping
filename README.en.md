# slam_gmapping

#### Description
slam_gmapping contains a wrapper around gmapping which provides SLAM capabilities.

#### Software Architecture
Software architecture description

slam_gmapping 

input:
```
slam_gmapping
├── gmapping
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   ├── launch
│   ├── nodelet_plugins.xml
│   ├── package.xml
│   ├── src
│   └── test
└── slam_gmapping
    ├── CHANGELOG.rst
    ├── CMakeLists.txt
    └── package.xml

```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-catkin/ros-noetic-ros-slam_gmapping-1.4.2-0.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-catkin/ros-noetic-ros-slam_gmapping-1.4.2-0.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-slam_gmapping-1.4.2-0.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-slam_gmapping-1.4.2-0.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

```
slam_gmapping
├── cmake.lock
├── env.sh
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
└── setup.zsh

```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
